import re

with open("inputs/04.txt", "r") as file:
    cards = file.read().splitlines()

regex_card = re.compile(r"Card +(\d+)")
regex_nb = re.compile(r"(\d+)")

def number_count(card: str) -> int:
    part1, part2 = card.split(":")[1].split("|")
    return len(set(regex_nb.findall(part1)) & set(regex_nb.findall(part2)))

def count_points(card: str) -> int:
    nb = number_count(card)
    return 2 ** (nb - 1) if nb else 0

# part 1
print(f"Sum of points: {sum(count_points(card) for card in cards)}")

# part 2
copies = [1] * len(cards)
for card in cards:
    card_nb = int(regex_card.search(card).group(1))
    nb = card_nb + number_count(card)
    copies[card_nb:nb] = [x + copies[card_nb - 1] for x in copies[card_nb:nb]]

print(f"Sum of copies: {sum(copies)}")
