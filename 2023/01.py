import re

# first part
print(sum(int(line[0] + line[-1]) for line in (re.sub(r'[a-zA-Z\s]', '', line) for line in open("inputs/01.txt", "r"))))

# second part
letters = {
    "one": "1", "two": "2", "three": "3",
    "four": "4", "five": "5", "six": "6",
    "seven": "7", "eight": "8", "nine": "9"
}
str_to_str = lambda s: letters.get(s, s)

sum_number = 0
with open("inputs/01.txt", "r") as file:
    for line in file:
        results = [m for m in re.findall(r"(?=(one|two|three|four|five|six|seven|eight|nine|[0-9]))", line)]
        sum_number += int(str_to_str(results[0]) + str_to_str(results[-1]))

print(sum_number)