import re

with open("inputs/02.txt", "r") as f:
    lines = f.readlines()


# Part 1
game_sum =  0
maximum = {"red": 12, "green": 13, "blue": 14}
for line in lines:
    if not any(int(number) > maximum[color] for number, color in re.findall(r"(\d+) (\w+)", line)):
        game_sum += int(re.match(r"Game (\d+):", line).group(1))

print(game_sum)

# Part 2
power_number = 0
for line in lines:
    result = re.findall(r"(\d+) (\w+)", line)
    
    colors = {"red": [], "green": [], "blue": []}
    for number, color in result:
        colors[color].append(int(number))
    
    power_number += max(colors["red"]) * max(colors["green"]) * max(colors["blue"])

print(power_number)
