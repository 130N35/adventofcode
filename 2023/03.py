import math
import time

def check_surroundings(x: int, y: int) -> bool:
    for i in range(x-1, x+2):
        for j in range(y-1, y+2):
            try:
                if table[i][j] not in '0123456789.':
                    return (i, j)
            except IndexError:
                pass
    return None

start_time = time.time()

table = []
with open (f'inputs/03.txt') as f:
    table = [line.strip() for line in f]

all_parts = {}
following_numbers = []
to_put_in = tuple()

for i, row in enumerate(table):
    for j, cell in enumerate(row):
        if cell.isdigit():
            following_numbers.append(cell)
            if (result := check_surroundings(i, j)) and not to_put_in:
                to_put_in = result
        else:
            if to_put_in:
                integer = int(''.join(following_numbers))
                all_parts.setdefault(to_put_in, []).append(integer)
            following_numbers, to_put_in = [], tuple()

print(sum(value for values in all_parts.values() for value in values))
print(sum(math.prod(values) for values in all_parts.values() if len(values) == 2))

end_time = time.time()
execution_time = end_time - start_time

print(f"Execution time: {execution_time} seconds")
